# frozen_string_literal: true
require_relative '_api'

# Zendesk API endpoints
class Zendesk
  def initialize(instance_type)
    @api = Api.new(instance_type)
  end

  def ticket_id(id)
    json = @api.get "tickets/#{id}"
    if !json || json.empty?
      puts "Ticket id #{id} failed"
      exit
    end
    json['ticket']
  end

  def ticket_search(value)
    json = @api.get 'search', query: "type:ticket #{value}"
    if !json || json.empty? || json['results'].empty?
      puts "Ticket search #{value} failed"
      exit
    end
    json['results'][0]
  end
end
