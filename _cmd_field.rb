# frozen_string_literal: true

class CmdField
  attr_reader :regex, :tests, :name

  REGEX = /^\s*(field)\s+("(?:\\"|[^"])*"|\w+)\s+(==|!=)\s+("(?:\\"|[^"])*"|\w+)/ # field <value> == <value>

  TESTS = [
    { line: 'field foo == 123456', expect: { tag: 'field', field: 'foo', operator: '==', value: '123456' } },
    { line: 'field foo == "bar"', expect: { tag: 'field', field: 'foo', operator: '==', value: 'bar' } },
    { line: 'field foo == "bar baz"', expect: { tag: 'field', field: 'foo', operator: '==', value: 'bar baz' } },
  ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'field'
  end
end