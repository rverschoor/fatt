# FATT

Form And Ticket Tester

## WIP!

## Usage

The application expects the name of a file which contains the script commands.

Example:
```shell
ruby fatt.rb script.fatt
```

To run only the internal self-test, pass `selftest` instead of a file name.

Example:
```shell
ruby fatt.rb selftest
```

## Script file

- The script file can have any extension.
- There should be one command per line.
- Leading and trailing whitespace is ignored.
- Commands are case-sensitive.
- If a value contains spaces, it must be enclosed in double quotes.

## Form Commands

TODO

## Ticket Commands

### `zendesk <production|sandbox>`

Description:\
Specify which Zendesk instance needs to be used for the tests.\
The server URL, user name, and token are read from the environment.\
Sandbox: `ZD_SB_URL`, `ZD_SB_USERNAME`, and `ZD_SB_TOKEN`\
Production: `ZD_URL`, `ZD_USERNAME`, and `ZD_TOKEN`

Example:
```text
zendesk production
zendesk sandbox
```

### `ticket id <id>`

Description:\
Search ticket by id.

Example:
```text
ticket id 123456
```

### `ticket search <text>`

Description:\
Search ticket by text.\
The text must be surrounded by double quotes.
Only the first matching ticket is used.

Example:
```text
ticket search "signature @@123"
```

### `status < == | != > <new|open|pending|closed|onhold>`

Description:\
Test then ticket status.\
It must be one of `new`, `open`, `pending`, `closed`, or `onhold`.

Example:
```text
status == new
status != open
```

### `tag < == | != > <tagname>`

Description:\
Test if a tag is present (`==`) or not (`!=`).

Example:
```text
tag == premium_customer
tag != stage-nrt
```

### `field <name> < == | != > <value>`

Description:\
Test if a field has a value (`==`) or not (`!=`).

Example:
```text
TODO
```

### `#`

Description:\
A comment line.\
The `#` must be the first non-whitespace character on the line.\
You can't have a command followed by a comment on the same line.

Example:
```text
# This is a comment
```

## TODO

- `test group <name>`
- `test name <name>`
- `comment == <text>`
- `comment != <text>`
- `sla policy == <value>`
