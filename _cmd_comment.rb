# frozen_string_literal: true

class CmdComment
  attr_reader :regex, :tests, :name

  REGEX = /^(#).*/ # comment

  TESTS = [
    { line: '#', expect: nil },
    { line: '  #', expect: nil },
    { line: '#  ', expect: nil },
    { line: '  #  ', expect: nil },
    { line: '# foo', expect: nil },
    { line: '  # foo', expect: nil },
  ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'comment'
  end
end