# frozen_string_literal: true

class CmdTag
  attr_reader :regex, :tests, :name

  REGEX = /^\s*(tag)\s*(==|!=)\s*("(?:\\"|[^"])*"|[\w\-]+)/ # tag == <value>

  TESTS = [
    { line: 'tag == 1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag != 1', expect: { tag: 'tag', operator: '!=', value: '1' } },
    { line: '  tag == 1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag == 1  ', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: '  tag == 1  ', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag   ==   1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag==1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag ==1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag== 1', expect: { tag: 'tag', operator: '==', value: '1' } },
    { line: 'tag == "foo"', expect: { tag: 'tag', operator: '==', value: 'foo' } },
    { line: 'tag == foo', expect: { tag: 'tag', operator: '==', value: 'foo' } },
    { line: '  tag == "foo"', expect: { tag: 'tag', operator: '==', value: 'foo' } },
    { line: 'tag == "foo"  ', expect: { tag: 'tag', operator: '==', value: 'foo' } },
    { line: '  tag == "foo"  ', expect: { tag: 'tag', operator: '==', value: 'foo' } },
    { line: 'tag == "foo bar"', expect: { tag: 'tag', operator: '==', value: 'foo bar' } },
    { line: 'tag == "foo_bar"', expect: { tag: 'tag', operator: '==', value: 'foo_bar' } },
    { line: 'tag == "foo-bar"', expect: { tag: 'tag', operator: '==', value: 'foo-bar' } },
    { line: 'tag == foo_bar', expect: { tag: 'tag', operator: '==', value: 'foo_bar' } },
    { line: 'tag == foo-bar', expect: {tag: 'tag', operator: '==', value: 'foo-bar'} },
    { line: 'tag == foo-', expect: {tag: 'tag', operator: '==', value: 'foo-'} },
    { line: 'tag == -foo', expect: {tag: 'tag', operator: '==', value: '-foo'} },
    # Invalid lines
    { line: 'tag', expect: :fail },
    { line: 'tag ==', expect: :fail },
    { line: 'tag == foo bar', expect: :fail },
    { line: 'foo', expect: :fail },
    ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'tag'
  end
end