# frozen_string_literal: true

require 'bundler/inline'
gemfile do
  source 'https://rubygems.org'
  gem 'faraday'
  gem 'faraday-retry'
end

require 'faraday'

# Zendesk API service
class Api
  def initialize(instance_type)
    instance instance_type
    @conn = Faraday.new(ENV.fetch(@url)) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch(@user)}/token", ENV.fetch(@token)
      # config.response :logger, nil, { headers: false, bodies: false }  # Log request
    end
  end

  def get(endpoint, params = nil)
    resp = @conn.get endpoint, params
    handle_status resp
    handle_body resp
  end

  private

  def instance(instance_type)
    case instance_type
    when 'production'
      @url = 'ZD_URL'
      @user = 'ZD_USERNAME'
      @token = 'ZD_TOKEN'
    when 'sandbox'
      @url = 'ZD_SB_URL'
      @user = 'ZD_SB_USERNAME'
      @token = 'ZD_SB_TOKEN'
    end
  end

  def retry_options
    { retry_statuses: [429] } # Retry when hitting rate limit
  end

  def handle_body(resp)
    resp.body
  end

  def handle_status(resp)
    return unless resp.status != 200
    puts "FAIL: Status #{resp.status}"
    exit
  end
end
