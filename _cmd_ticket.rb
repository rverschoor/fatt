# frozen_string_literal: true

class CmdTicket
  attr_reader :regex, :tests, :name

  REGEX = /^\s*(ticket)\s+(id|search)\s+("(?:\\"|[^"])*"|\w+)/ # ticket id <id>

  TESTS = [
    { line: 'ticket id 123456', expect: { tag: 'ticket', type: 'id', value: '123456' } },
    { line: 'ticket search "foo"', expect: { tag: 'ticket', type: 'search', value: 'foo' } },
    { line: 'ticket search "foo bar"', expect: { tag: 'ticket', type: 'search', value: 'foo bar' } },
  ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'ticket'
  end
end