# frozen_string_literal: true

require_relative '_zendesk'

class Action
  def initialize
    @expression = nil
    @ticket_info = nil
  end

  def do(expression)
    @expression = expression
    case expression[:tag]
    when 'tag'
      do_tag
    when 'status'
      do_status
    when 'field'
      do_field
    when 'zendesk'
      do_zendesk
    when 'ticket'
      do_ticket
    else
      puts "Unhandled tag #{expression[:tag]}"
      exit
    end
  end

  def do_tag
    has_tag = @ticket_info['tags'].include? @expression[:value]
    case @expression[:operator]
    when '=='
      if has_tag
        puts "OK tag==#{@expression[:value]}"
      else
        puts "NOK tag==#{@expression[:value]}"
      end
    when '!='
      if !has_tag
        puts "OK tag!=#{@expression[:value]}"
      else
        puts "NOK tag!=#{@expression[:value]}"
      end
    end
  end

  def do_status
    p '@@@ status'
  end

  def do_field
    p '@@@ field'
  end

  def do_zendesk
    @zd = Zendesk.new(@expression[:zendesk])
  end

  def do_ticket
    case @expression[:type]
    when 'id'
      @ticket_info = @zd.ticket_id @expression[:value]
    when 'search'
      @ticket_info = @zd.ticket_search @expression[:value]
    else
      puts "Unknown ticket type: #{@expression[:type]}"
      exit
    end
  end
end