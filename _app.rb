# frozen_string_literal: true

require_relative '_lexer'
require_relative '_parser'
require_relative '_action'
require_relative '_commands'

class App
  def initialize
    check_arguments
    @selftest_only = check_selftest
    @fname = script_file unless @selftest_only
    @commands = commands
    @action = Action.new
    selftests
  end

  def go
    %i[dryrun realrun].each do |stage|
      lines.each do |line, line_nr|
        process_line line, line_nr, stage
      end
    end
  end

  private

  def check_arguments
    usage unless ARGV.length == 1
  end

  def check_selftest
    ARGV[0] == 'selftest'
  end

  def script_file
    fname = ARGV[0]
    check_file fname
    fname
  end

  def usage
    puts "FATT v#{VERSION}"
    puts 'Usage: ./fatt.rb <scriptfile>'
    puts '   eg: ./fatt.rb test_sla.fatt'
    exit
  end

  def check_file(fname)
    return if File.file? fname
    puts "File #{fname} doesn't exist"
    exit
  end

  def lines(&block)
    File.foreach(@fname).with_index(&block)
  end

  def commands
    {
      tag: CmdTag.new,
      comment: CmdComment.new,
      status: CmdStatus.new,
      field: CmdField.new,
      zendesk: CmdZendesk.new,
      ticket: CmdTicket.new
    }
  end

  def selftests
    puts 'Running self-tests' if @selftest_only
    success = true
    commands.each_value do |command|
      command.tests.each_with_index do |test, line_nr|
        line = test[:line]
        expect = test[:expect]
        tokens = process_line line, line_nr, :selftest
        unless selftest command.name, test, line_nr, expect, tokens
          success = false
        end
      end
    end
    puts 'Self-tests finished' if @selftest_only
    exit if @selftest_only || !success
  end

  def process_line(line_in, line_nr, stage)
    line = line_in.strip.chomp
    expression = line_to_expression line, line_nr, stage
    return expression if stage == :selftest
    sanity_check line, line_nr, expression
    return unless expression && stage == :realrun
    @action.do expression
  end

  def line_to_expression(line, line_nr, stage)
    lexer = Lexer.new stage, @commands
    tokens = lexer.tokenize(line, line_nr)
    parser = Parser.new(tokens)
    parser.parse
  end

  def selftest(command, test, line_nr, expect, tokens)
    valid = tokens&.length == expect&.length
    valid = compare_tokens tokens, expect if valid
    return true if valid
    failed_test command, test, line_nr, tokens
    false
  end

  def compare_tokens(tokens, expect)
    valid = true
    return valid if tokens == :fail && expect == :fail
    tokens&.each do |key, value|
      valid = expect[key] == value
      break unless valid
    end
    valid
  end

  def failed_test(command, test, index, tokens)
    puts "FAIL #{command}:#{index + 1}"
    puts "  Input: >#{test[:line]}<"
    puts "   Want: #{test[:expect]}"
    puts "    Got: #{tokens}"
  end

  # Prevent that scriptlines are silently skipped due to an incorrect regex
  def sanity_check(line, line_nr, expression)
    return if expression
    # Only comments and blank lines should return a nil expression
    return unless line.length.positive? && line[0] != '#'
    puts 'Scriptline skipped in error'
    puts "#{line_nr}: #{line}"
    exit
  end
end
