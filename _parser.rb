# frozen_string_literal: true

class Parser
  def initialize(tokens)
    @tokens = tokens
  end

  def parse
    return :fail if @tokens == :fail
    parsed = nil
    until @tokens.empty?
      expression = parse_expression
      parsed = expression if expression
    end
    parsed
  end

  private

  def parse_expression
    token = @tokens.shift
    case token[0]
    when 'tag'
      { tag: 'tag', operator: token[1], value: unquote(token[2]) }
    when 'status'
      { tag: 'status', operator: token[1], value: unquote(token[2]) }
    when 'field'
      { tag: 'field', field: token[1], operator: token[2], value: unquote(token[3]) }
    when 'zendesk'
      { tag: 'zendesk', zendesk: unquote(token[1]) }
    when 'ticket'
      { tag: 'ticket', type: token[1], value: unquote(token[2]) }
    when '#'
      nil
    when nil
      nil
    else
      puts "Parser: Unhandled token: #{token[0]}"
      exit
    end
  end

  def unquote(value)
    value.gsub(/\A"|"\Z/, '')
  end
end
