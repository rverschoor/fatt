# frozen_string_literal: true

class CmdZendesk
  attr_reader :regex, :tests, :name

  REGEX = /^\s*(zendesk)\s+("(?:\\"|[^"])*"|(sandbox|production))/ # zendesk <sandbox|production>

  TESTS = [
    { line: 'zendesk sandbox', expect: { tag: 'zendesk', zendesk: 'sandbox' } },
    { line: 'zendesk "production"', expect: { tag: 'zendesk', zendesk: 'production' } },
  ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'zendesk'
  end
end