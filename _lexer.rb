# frozen_string_literal: true

class Lexer
  def initialize(stage, commands)
    @regexs = initialize_regexs commands
    @selftest = stage == :selftest
  end

  def tokenize(line, line_nr)
    @input = line
    @line_nr = line_nr + 1
    @tokens = []
    until @input.empty?
      matched = false
      @regexs.each do |pattern|
        next unless (match = pattern.match(@input))
        matched = true
        @tokens << match.captures
        @input = match.post_match
      end
      next if matched
      return :fail if @selftest
      unexpected_token
    end
    @tokens
  end

  private

  def initialize_regexs(commands)
    regexs = []
    commands.each_value do |command|
      regexs << command.regex
    end
    regexs
  end

  def unexpected_token
    puts 'Tokenize: Unexpected token:'
    puts "line# #{@line_nr}: #{@input}"
    exit
  end
end
