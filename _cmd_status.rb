# frozen_string_literal: true

class CmdStatus
  attr_reader :regex, :tests, :name

  REGEX = /^\s*(status)\s+(==|!=)\s+("(?:\\"|[^"])*"|(new|open|pending|closed|onhold))/ # status == <new|open|pending|closed|onhold>

  TESTS = [
    { line: 'status == open', expect: { tag: 'status', operator: '==', value: 'open' } },
    { line: 'status == "new"', expect: { tag: 'status', operator: '==', value: 'new' } },
  ].freeze

  def initialize
    @regex = REGEX
    @tests = TESTS
    @name = 'status'
  end
end