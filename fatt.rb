#! /usr/bin/env ruby
# frozen_string_literal: true

require_relative '_app'

VERSION = 0.0

app = App.new
app.go
